{-|
  hanoi     Towers of Hanoi

  Usage:    hanoi [NUM-DISKS]

  Examples: hanoi     # use default 5
            hanoi 2
            hanoi -h

  Compile:  ghc --make -Wall hanoi.hs
  Lint:     hlint -s hanoi.hs

  Author:   haskell@wu6ch.de

  Inspired by:

  "Tower of Hanoi"
  https://en.wikipedia.org/wiki/Tower_of_Hanoi
-}


import Control.Monad ((>=>), void)
import Control.Monad.State (StateT, execStateT, get, liftIO, modify, put)

import Data.Array
import Data.Bool (bool)
import Data.List (intercalate, transpose)
import Data.Maybe (fromMaybe)

import System.Environment (getArgs)

import Text.Read (readMaybe)


type NumDisks    = Int
type TowerHeight = Int

type Tower       = [NumDisks]
type TowerArray  = Array TowerIndex Tower


data TowerIndex
  = Tower1
  | Tower2
  | Tower3
  deriving (Eq, Ix, Ord, Show)

data HanoiState =
  HanoiState
    { hsCnt :: Int
    , hsHeight :: TowerHeight
    , hsTowers :: Either String TowerArray
    }


defaultNumDisks :: NumDisks
defaultNumDisks = 5


towerChar :: Char
towerChar = '#'


setTowerHeight :: NumDisks -> TowerHeight
setTowerHeight = id


setTower :: NumDisks -> Tower
setTower numDisks = [1 .. numDisks]


setTowerArray :: NumDisks -> TowerArray
setTowerArray numDisks =
  array (Tower1, Tower3) [(Tower1, setTower numDisks), (Tower2, []), (Tower3, [])]


setInitialState :: NumDisks -> StateT HanoiState IO ()
setInitialState numDisks = put hanoiState
  where
    hanoiState =
      HanoiState
        { hsCnt    = 0
        , hsHeight = setTowerHeight numDisks
        , hsTowers = Right $ setTowerArray numDisks
        }


alignTowers :: TowerHeight -> [Tower] -> [Tower]
alignTowers height towers = align <$> towers
  where
    align :: Tower -> Tower
    align tower = replicate (height - length tower) 0 <> tower


towerToStrings :: Tower -> [String]
towerToStrings tower = toString <$> tower
  where
    toString :: NumDisks -> String
    toString 0    = "|"
    toString disk = replicate (2 * disk - 1) towerChar


towersToStrings :: TowerHeight -> [Tower] -> [[String]]
towersToStrings height = transpose . map towerToStrings . alignTowers height


alignDisk :: Int -> String -> String
alignDisk width disk = mconcat [spaces, disk, spaces]
  where
    spaces    = replicate numSpaces ' '
    numSpaces = (width - length disk) `div` 2


printDisks :: Int -> [String] -> IO ()
printDisks width disks = do
  let alignedDisks = alignDisk width <$> disks
      diskLine     = intercalate "  " alignedDisks
  (putStrLn . reverse . dropWhile (== ' ') . reverse) diskLine


printTowers :: StateT HanoiState IO ()
printTowers = do
  hanoiState <- get
  let towers = hsTowers hanoiState
  case towers of
    Left  _  -> return ()
    Right ts -> do
      let towerList  = elems ts
          height     = hsHeight hanoiState
          width      = 2 * height + 1
          bottom     = replicate width '-'
          bottoms    = replicate (length ts) bottom
          bottomLine = intercalate "  " bottoms
      liftIO $ putStrLn ""
      liftIO $ mapM_ (printDisks width) $ towersToStrings height towerList
      liftIO $ putStrLn bottomLine
      liftIO $ putStrLn ""


printResult :: NumDisks -> StateT HanoiState IO ()
printResult numDisks = do
  hanoiState <- get
  let expected = fromInteger $ 2 ^ numDisks - 1
      actual   = hsCnt hanoiState
      (e, a)   = (show expected, show actual)
      diffMsg  = bool "" (mconcat [" , but expected: ", e, "!"]) (actual /= expected)
      towers   = hsTowers hanoiState
  case towers of
    Left err -> liftIO $ putStrLn err
    Right _  -> return ()
  liftIO $ putStrLn $ mconcat ["Number of moves: ", a, diffMsg]


popTower :: TowerIndex -> TowerArray -> Either String (Int, TowerArray)
popTower tId towers =
  if poppedVal == 0
    then Left $ mconcat ["pop: ", show tId, " is empty!\n"]
    else Right (poppedVal, poppedTowers)
  where
    tower        = towers ! tId
    poppedVal    = bool (head tower) 0 (null tower)
    poppedTowers = towers // [(tId, drop 1 tower)]


pushTower :: TowerIndex -> (Int, TowerArray) -> Either String TowerArray
pushTower tId (pushVal, towers) =
  if headVal /= 0 && headVal <= pushVal
    then Left $ mconcat ["push: disk of ", show tId, " not larger!\n"]
    else Right pushedTowers
  where
    tower        = towers ! tId
    headVal      = bool (head tower) 0 (null tower)
    pushedTowers = towers // [(tId, pushVal : tower)]


move :: TowerIndex -> TowerIndex -> StateT HanoiState IO ()
move srcTower destTower = do
  hanoiState <- get
  let m      = show $ hsCnt hanoiState + 1
      (s, d) = (show srcTower, show destTower)
      towers = hsTowers hanoiState
  case towers of
    Left _   -> return ()
    Right ts -> do
      liftIO $ putStrLn $ mconcat [m, ": Move from ", s, " to ", d, ":"]
      let movedTowers = (popTower srcTower >=> pushTower destTower) ts
      modify (\state -> state {hsCnt = hsCnt state + 1, hsTowers = movedTowers})


{-|
  The function moves 'n' disks recursively from 'a' via 'b' to 'c'.
-}
hanoi :: NumDisks -> TowerIndex -> TowerIndex -> TowerIndex -> StateT HanoiState IO ()
hanoi 0 _ _ _ = return ()
hanoi n a b c = do
  hanoi (n - 1) a c b
  move a c
  printTowers
  hanoi (n - 1) b a c


playHanoi :: NumDisks -> StateT HanoiState IO ()
playHanoi numDisks = do
  liftIO $ putStrLn $ "\nNum of disks: " <> show numDisks
  setInitialState numDisks
  printTowers
  hanoi numDisks Tower1 Tower2 Tower3
  printResult numDisks


readNumDisks :: [String] -> NumDisks
readNumDisks [] = defaultNumDisks
readNumDisks args = fromMaybe defaultNumDisks $ readMaybe $ head args


main :: IO ()
main = do
  args <- getArgs
  if "-h" `elem` args
     then putStrLn usage
     else void (execStateT (playHanoi $ readNumDisks args) dummy)
  where
    usage = "Usage: hanoi [NUM-DISKS]"
    dummy = HanoiState 0 0 $ Left ""


-- EOF
