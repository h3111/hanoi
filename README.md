# _Towers of Hanoi_ in Haskell


This program is inspired by
[_Tower of Hanoi_](https://en.wikipedia.org/wiki/Tower_of_Hanoi).


## Compiling and Linting

Since `hanoi.hs` does not use "special" modules, the compilation should be
possible with the standard Haskell installation:

````bash
$ sudo apt install haskell-platform hlint
$ ghc --make -Wall hanoi.hs
````

Lint:

````bash
$ hlint -s hanoi.hs
````


### "Installation"

Copy `hanoi` to a directory that is listed in the environment variable `PATH`,
e.g. `~/bin` (or create a symlink).


### Usage

Call `hanoi` this way to get a short usage message:

````bash
$ hanoi -h
Usage: hanoi [NUM-DISKS]
````

Example game session (with two disks):

````
$ hanoi 2

Num of disks: 2

  #      |      |
 ###     |      |
-----  -----  -----

1: Move from Tower1 to Tower2:

  |      |      |
 ###     #      |
-----  -----  -----

2: Move from Tower1 to Tower3:

  |      |      |
  |      #     ###
-----  -----  -----

3: Move from Tower2 to Tower3:

  |      |      #
  |      |     ###
-----  -----  -----

Number of moves: 3
````


<sub>[Wolfgang](mailto:haskell@wu6ch.de)</sub>


<!-- EOF -->
